<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Etat;

class EtatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $data=[
            [
                'intitule'=>'en attente de confirmation',
                'description'=>'en attente de confirmation'
            ],
            [
                'intitule'=>'confirmée',
                'description'=>'confirmée'
            ],
            [
                'intitule'=>'envoyée',
                'description'=>'envoyée'
            ],
            [
                'intitule'=>'payée',
                'description'=>'payée'
            ],
            [
                'intitule'=>'retournée',
                'description'=>'retournée'
            ],

        ];

        Etat::insert($data);
    }
}
