<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\CategorieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[HomeController::class,"index"])->name('home.index');
Route::get('/panier',[HomeController::class,"show_panier"])->name('home.panier');
Route::get('/panier/clear',[HomeController::class,"clear"])->name('home.clear');
Route::post('panier/add/{id}',[HomeController::class,"add"])->name('home.add');
Route::delete('panier/delete/{id}',[HomeController::class,"delete"])->name('home.delete');
Route::get('/catalogue/search',[HomeController::class,'search'])->name("home.search");
Route::get('/commandes/search',[CommandeController::class,"search"])->name('commandes.search');
Route::get('/commandes/exportCSV',[CommandeController::class,"exportCSV"])->name('commandes.exportCSV');
Route::resource("categories",CategorieController::class);
Route::resource("clients",ClientController::class);
Route::resource("produits",ProduitController::class);
Route::resource("commandes",CommandeController::class);

