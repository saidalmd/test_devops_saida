<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Produit;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function search(Request $request){
        $list_categories=Categorie::all();
        $des=$request->query("designation");
        $prix_min=$request->query("prix_min");
        $prix_max=$request->query("prix_max");
        $qte=$request->query("qte");
        $categorie=$request->query("categorie");

        $p=Produit::query();
        if($des){
            $p->where("designation","like","%".$des."%");
        }
        if($prix_min){
            $p->where("prix_u",">",$prix_min);
        }
        if($prix_max){
            $p->where("prix_u","<",$prix_max);
        }
        if($qte){
            $p->where("quantite_stock",">",$qte);
        }
        if($categorie){
            $p->where("categorie_id","=",$categorie);
        }
        $produits=$p->paginate(6);
        $produits->appends([
            'designation'=>$des,
            'prix_min'=>$prix_min,
            'prix_max'=>$prix_max,
            'qte'=>$qte,
            'categorie'=>$categorie,
        ]);
        return view("home.index",compact('produits','list_categories'));

    }
    public function index(){
        
        $produits=Produit::with('categorie')->paginate(6);
        $list_categories=Categorie::all();
        return view('home.index',compact('produits','list_categories'));
    }
    public function add(Request $request,$id){
        $qte=$request->input('qte',1);
        $produit=Produit::find($id);
        $panier=$request->session()->get('panier',[]);
        if(isset($panier[$id])){
            $panier[$id]['qte']=$qte;
        }else{
        $panier[$id]=[
            'qte'=>$qte,
            'produit'=>$produit
        ];
    }
    
    $request->session()->put('panier',$panier);
    return redirect()->back();
        
    }
    public function show_panier(Request $request){
        $panier=$request->session()->get('panier',[]);
        $tot=0;
        foreach($panier as $id=>$item){
            $tot+=$item['qte']*$item['produit']->prix_u;
        }
        return view('home.panier',compact('panier','tot'));
    }
    public function delete(Request $request,$id){
        $panier=$request->session()->get('panier',[]);
        unset($panier[$id]);
        $request->session()->put('panier',$panier);
        return redirect()->back();
    }
    public function clear(Request $request ){

        $request->session()->forget('panier');
        // return redirect()->back();
        return redirect()->route('home.index');

    }
}