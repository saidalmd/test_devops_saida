<?php

namespace App\Http\Controllers;
use App\Models\Etat;
use App\Models\Client;
use App\Models\Produit;
use App\Models\Commande;
use Illuminate\Http\Request;
use App\Models\LigneDeCommande;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Response;

class CommandeController extends Controller
{
     /**
     * importer csv
     */
    public function exportCSV(Request $request)
    {
        //
        $commandes=Commande::all();
        $csvFilaName='commandes.csv';
        
        $headers=[
            'Content-Type'=>'text/csv',
            'Content-Disposition'=>'attachement; filename="'.$csvFilaName.'"',
        ];

        $handle=fopen('php://output','w');
        fputcsv($handle,['Id','Date','Nom','Prenom','Ville','Tele','Total']);

        foreach($commandes as $cmd){
            $sum=0;
            $lignes_cmd=LigneDeCommande::where('commande_id','=',$cmd->id)->get();
            foreach($lignes_cmd as $l){
                $sum+=($l->quantite)*($l->prix_vente);
            }
            fputcsv($handle,[
                $cmd->id,
                $cmd->datetime,
                $cmd->client->nom,
                $cmd->client->prenom,
                $cmd->client->ville,
                $cmd->client->tele,
                $sum,
            ]);
        };

        fclose($handle);
        return Response::make('',200,$headers);
        
    }

    /**
     * chercher une commande 
     */
    public function search(Request $request)
    {
        //
        $list_etats=Etat::all();
        $date_min=$request->query('date_min');
        $date_max=$request->query('date_max');
        $etat=$request->query('etat');
        $client=$request->query('client');

        $c=Commande::query();
        if($date_min){
            $c->whereDate('datetime','>',$date_min);
        }
        if($date_max){
            $c->whereDate('datetime','<',$date_max);
        }
        if($etat){
            $c->where('etat_id',$etat);
        }
        if($client) {
            $c->whereHas('client', function ($query) use ($client) {
                $query->where('nom', '=',$client);
            });
        }
        $commandes=$c->paginate(5);
        $commandes->appends([
            'client'=>$client,
            'etat'=>$etat,
            'date_min'=>$date_min,
            'date_max'=>$date_max,
        ]);
        return view('commandes.index',compact('commandes','list_etats'));
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $commandes=Commande::with('etat')->with('client')->paginate(5);
        $list_etats=Etat::all();
        return view('commandes.index',compact('commandes','list_etats'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'tele'=>'required|unique:clients,tele',
            'ville'=>'required',
            'adresse'=>'required'
        ]);
        $client=Client::create($request->all());
        $cmd=Commande::create(['datetime'=>now(),'client_id'=>$client->id,'etat_id'=>1]);
        $panier=$request->session()->get('panier');
        foreach($panier as $item){
            LigneDeCommande::create([
                'produit_id'=>$item['produit']->id,
                'commande_id'=>$cmd->id,
                'quantite'=>$item['qte'],
                'prix_vente'=>$item['produit']->prix_u,
            ]);
            $produit=Produit::find($item['produit']->id);
            $produit->quantite_stock-=$item['qte'];
            $produit->save();
        }
        return view('commandes.confirmation');


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
