 @extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')
     
    <h1>Catalogue</h1>
    <form action="{{route('home.search')}}" id="recherche_catalogue">
        <input type="text" name="designation" id="designation" placeholder="designation...">
        <input type="number" name="prix_min" id="prix_min" placeholder="prix min...">
        <input type="number" name="prix_max" id="prix_max" placeholder="prix max...">
        <input type="text" name="qte" id="qte" placeholder="quantite min..." >
        <select name="categorie" id="categorie">
            <option value="">Choisissez...</option>
            @foreach ($list_categories as $item)
                <option value="{{$item->id}}">{{$item->designation}}</option>
                
            @endforeach
        </select>
        <input type="submit" value="Rechercher">
    </form>
    <div class="catalogue py-3" style="width:80%;">
   
   @foreach ($produits as $item)
       
       <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('storage/'.$item->image)}}" alt="" style="height:300px;">
            <div class="card-body">
                <h5 class="card-title">{{$item->designation}}</h5>
                <p class="card-text">Prix : {{$item->prix_u}} MAD</p>
                @if ($item->quantite_stock==0)
                    <p>En repture de stock</p>
                @else
                <p>En stock : {{$item->quantite_stock}}</p>
                <form action="{{route('home.add',["id"=>$item->id])}}" method='POST'>
                @csrf
                    <label for="qte">Quantite</label>
                    <input type="number" name="qte" id="qte" min="1" max="{{$item->quantite_stock}}">
                    <input type="submit" value="Acheter" class="btn btn-primary">
                </form>
                @endif
            </div>
        </div>
 
   @endforeach
     
    </div>
    {{$produits->links()}}

@endsection