@extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')
 <div id="recherche_commandes">
 <form action="{{route('commandes.search')}}" method="get">
    <input type="text" name="client" id="client" placeholder="client...">
    <input type="date" name="date_min" id="date_min" placeholder="date min...">
    <input type="date" name="date_max" id="date_max" placeholder="date max...">
    <select name="etat" id="etat">
        <option value="">Choisissez...</option>
        @foreach ($list_etats as $item)
            <option value="{{$item->id}}">{{$item->intitule}}</option>
        @endforeach
    </select>
    <input type="submit" value="Rechercher">
 </form>
 <a href="{{route('commandes.exportCSV')}}" class="btn btn-primary mt-2">Exporter csv</a>
</div>
 
 <h1>Liste des commandes</h1>
 <table id="tbl">
   <tr>
       <th>Id</th>
     <th>Date</th>
     <th>Client</th>
     <th>Etat</th>
     <th colspan="3">Actions</th>
   </tr>
   @foreach ($commandes as $cmd)
       <tr>
         <td>{{$cmd->id}}</td>
         <td>{{$cmd->datetime}}</td>
         <td>{{$cmd->client->nom}} {{$cmd->client->prenom}}</td>
         <td>{{$cmd->etat->intitule}}</td>
         <td><a href="#">Details</a></td>
       </tr>
   @endforeach
 </table>
 {{$commandes->links()}}
    
@endsection
