@extends('layouts.admin')
 @section('title','Modifier un produit')
 @section('content')
    <form action="{{route('produits.update',$produit->id)}}" method="post" enctype="multipart/form-data">
        @method('put')
        @csrf
    <div>
        <label for="designation">Designation</label>
        <input type="text" id="designation" name="designation" value="{{old('designation',$produit->designation)}}">
    </div>
    <div>
        <label for="prix_u">Prix unitaire</label>
        <input type="text" id="prix_u" name="prix_u" value="{{old('prix_u',$produit->prix_u)}}">
    </div>
    <div>
        <label for="quantite_stock">Quantite stock</label>
        <input type="number" min=0 max=9999 id="quantite_stock" name="quantite_stock" value="{{old('quantite_stock',$produit->quantite_stock)}}">
    </div>
    <div>
        <label for="categorie">Categorie</label>
        <select name="categorie_id" id="categorie" value="{{old('categorie_id',$produit->categorie_id)}}">
            @foreach($list_categories as $cat)
                <option value="{{$cat->id}}">{{$cat->designation}}</option>
            @endforeach
        </select>
    </div>
    <div>
        <label for="image">Image</label>
        <input type="file" name="image" id="image" accept="images/*">
    </div>
    <div>
        <input type="submit" value="Modifier">
    </div>
    </form>
    
@endsection
