@extends('layouts.admin')
 @section('title','Afficher tous les produits')
 @section('content')
    <div>
      <form action="{{route('produits.index')}}" method="get">
        
        
        <input type="text" name="designation" id="designation" placeholder="designation.." >
        <input type="text" name="prix_u_min" id="prix_u_min" placeholder="prix unitaire minimale.." >
        <input type="text" name="prix_u_max" id="prix_u_max" placeholder="prix unitaire maximale.." >
        <input type="text" name="quantite_stock" id="quantite_stock" placeholder="quantite_stock.." >
        <select name="categorie_id" id="categorie_id">
          <option value="">Choisir catégorie...</option>
          @foreach($list_categories as $cat)
            <option value="{{$cat->id}}">{{$cat->designation}}</option>
          @endforeach
        </select>
        <input type="submit" value="Rechercher">
      </form>
      <a href="{{route('produits.index')}}">Retour à la liste</a>
    </div>
<h1>Liste des produits</h1>
    <a href="{{route('produits.create')}}">Ajouter un nouveau produit</a>
    <table id="tbl">
      <tr>
          <th>Id</th>
        <th>Designation</th>
        <th>Prix unitaire</th>
        <th>Qauntite stock</th>
        <th>Catégorie</th>
        <th colspan="3">Actions</th>
      </tr>
      @foreach ($produits as $prod)
          <tr>
            <td>{{$prod->id}}</td>
            <td>{{$prod->designation}}</td>
            <td>{{$prod->prix_u}}</td>
            <td>{{$prod->quantite_stock}}</td>
            <td>{{$prod->categorie->designation}}</td>
            <td><a href="{{route('produits.show',$prod->id)}}">Details</a></td>
            <td><a href="{{route('produits.edit',$prod->id)}}">Modifier</a></td>
            <td>
                <form action="{{route('produits.destroy',$prod->id)}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer ce produit?')">
                </form></td>
          </tr>
      @endforeach
    </table>
    {{$produits->links()}}

@endsection