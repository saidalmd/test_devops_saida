<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Produit;
use App\Models\Categorie;
use Illuminate\Support\Facades\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategorieControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    use RefreshDatabase;
    /** @test */
    public function index_categories_affiche_categories(): void
    {   
        // arrange
        Categorie::factory()->count(3)->create();
        // act
        $response=$this->get('/categories');
        // assert
        // $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertViewHas('categories');

        $categories=Categorie::all();
        foreach($categories as $categorie){
            $response->assertSee($categorie->designation);
        }
    }

    /** @test */
    public function store_categories_enregistre_les_donnees_et_redirect_vers_index(){
        // arrange
        $data=[
            "designation"=>"electronique",
            "description"=>"electronique",
        ];
        // act
        $response=$this->post('/categories',$data);
        // assert
        $response->assertRedirect('/categories');
        $this->assertDatabaseHas('categories',$data);
    }

    /** @test */
    public function create_categories_retourne_vue_create(){
        // act
        $response=$this->get('/categories/create');
        // assert
        $response->assertViewIs('categories.create');
    }

    /** @test */
    public function show_categorie_retourne_vue_categorie_avec_variable_cat(){
        // arrange
        Categorie::factory()->count(1)->create();
        $id_cate=Categorie::first()->id;
        // act
        $response=$this->get('/categories/'.$id_cate);
        // assert
        $response->assertViewIs('categories.show');
        $response->assertViewHas('cat');
    }

    /** @test */
    public function edit_categorie_retourne_vue_edit_avec_variable_cat(){
        // arrange
        Categorie::factory()->count(1)->create();
        $id_cate=Categorie::first()->id;
        // act
        $response=$this->get('/categories/'.$id_cate.'/edit');
        // assert
        $response->assertViewIs('categories.edit');
        $response->assertViewHas('cat');
    }

    /** @test */
    public function update_categorie_modifie_une_categorie_redirige_vers_index(){
        // arrange
        Categorie::factory()->count(1)->create();
        $id_cate=Categorie::first()->id;
        $cate=Categorie::find($id_cate);
        $old_data=[
            "designation"=>$cate->designation,
            "description"=>$cate->description,
        ];
        $new_data=[
            "designation"=>"accessoires",
            "description"=>"accessoires",
        ];
        // act
        $response=$this->put('/categories/'.$id_cate,$new_data);
        // assert
        $response->assertRedirect('/categories');
        $this->assertDatabaseHas('categories',$new_data);
        $this->assertDatabaseMissing('categories',$old_data);
    }

    /** @test */
    public function destroy_categorie_supprime_une_categorie_redirige_vers_index(){
        // arrange
        Categorie::factory()->count(1)->create();
        $id_cate=Categorie::first()->id;
        $cate=Categorie::first();
        $old_data=[
            "designation"=>$cate->designation,
            "description"=>$cate->description,
        ];
        // act
        $response=$this->delete('/categories/'.$id_cate);
        // assert
        $response->assertRedirect('/categories');
        $this->assertDatabaseMissing('categories',$old_data);

    }
}
