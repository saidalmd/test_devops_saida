<?php

namespace Tests\Unit;

use App\Models\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieModelTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    /** @test */
    public function sommation_deux_nombres_positifs_retourne_un_resultat_positif(): void
    {
        $a=20;
        $b=10;
        $c=30;

        $categorie=new Categorie();
        $result=$categorie->sommation($a,$b);

        $this->assertEquals($c,$result);
    }
}
